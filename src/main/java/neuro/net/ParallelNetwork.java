package neuro.net;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Predicate;

class ParallelNetwork {
    private final ExecutorService executorService;
    private final List<FeedForwardNetwork> networks;
    private final Predicate<FeedForwardNetwork> stoppingConditionPredicate;

    public ParallelNetwork(List<FeedForwardNetwork> networks, Predicate<FeedForwardNetwork> stoppingCondition) {
        this.networks = networks;
        executorService = Executors.newFixedThreadPool(4);
        this.stoppingConditionPredicate = stoppingCondition;
    }

    public void start() throws ExecutionException, InterruptedException {
        while (!stoppingConditionPredicate.test(networks.get(0)))
            process();
        executorService.shutdown();
    }

    private void process() throws ExecutionException, InterruptedException {
        List<Future<FeedForwardNetwork>> futures = new ArrayList<>();
        for (FeedForwardNetwork network : networks) {
            Future<FeedForwardNetwork> future = executorService.submit(network);
            futures.add(future);
        }
        processFutures(futures);
    }

    private void processFutures(List<Future<FeedForwardNetwork>> futures) throws ExecutionException, InterruptedException {
        int count = 0;
        RealMatrix[][] weights = new RealMatrix[futures.size()][networks.get(0).getLayerCount()];
        RealVector[][] biases = new RealVector[futures.size()][networks.get(0).getLayerCount()];

        for (Future<FeedForwardNetwork> future : futures) {
            FeedForwardNetwork feedForwardNetwork = future.get();
            weights[count] = feedForwardNetwork.getWeights();
            biases[count] = feedForwardNetwork.getBiases();
            count += 1;
        }

        updateNetworks(sumAndAverage(weights), sumAndAverage(biases));
    }

    private void updateNetworks(RealMatrix[] weights, RealVector[] biases) {
        for (FeedForwardNetwork network : networks) {
            network.setWeights(weights);
            network.setBiases(biases);
        }
    }

    private RealMatrix[] sumAndAverage(RealMatrix[][] weights) {
        RealMatrix[] summedWeights = new RealMatrix[weights[0].length];
        for (int i = 0; i < weights[0].length; i++) {
            summedWeights[i] = weights[0][i];
            for (int j = 1; j < weights.length; j++) {
                summedWeights[i] = summedWeights[i].add(weights[j][i]);
            }
            summedWeights[i] = summedWeights[i].scalarMultiply(1 / (double) weights.length);
        }

        return summedWeights;
    }

    private RealVector[] sumAndAverage(RealVector[][] biases) {
        RealVector[] bias = new RealVector[biases[0].length];
        for (int i = 0; i < biases[0].length; i++) {
            bias[i] = biases[0][i];
            for (int j = 1; j < biases.length; j++) {
                bias[i] = bias[i].add(biases[j][i]);
            }
            bias[i] = bias[i].map(x -> x * (1 / (double) biases.length));
        }
        return bias;
    }
}
