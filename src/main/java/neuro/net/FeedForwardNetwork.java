package neuro.net;

import neuro.data.DataHolder;
import neuro.error.ErrorFunctions;
import neuro.layer.NetworkLayer;
import neuro.learn.LearningAlgorithm;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;

public class FeedForwardNetwork implements Callable<FeedForwardNetwork> {

    private static final Logger LOGGER = LogManager.getLogger(FeedForwardNetwork.class);
    private final List<Double> batchErrors;
    private final List<Double> accumulatedError;
    private final NetworkLayer start;
    private ErrorFunctions errorFunctions;
    private int batchSize;
    private List<DataHolder> dataSet;
    private NetworkLayer last;
    private LearningAlgorithm learningAlgorithm;
    private int epochCount = 1;
    private int recordCount;
    private Predicate<FeedForwardNetwork> stopAtCondition;
    private boolean enableInfo;
    private boolean stop = false;
    private int layerCount = 1;

    public FeedForwardNetwork(NetworkLayer networkLayer) {
        this.batchErrors = new ArrayList<>();
        this.start = networkLayer;
        this.accumulatedError = new ArrayList<>();
        setUpLayers(start.getNextLayer());
    }

    public void setErrorFunctions(ErrorFunctions errorFunctions) {
        this.errorFunctions = errorFunctions;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public void setDataSet(List<DataHolder> dataSet) {
        this.dataSet = dataSet;
    }

    public void setLearningAlgorithm(LearningAlgorithm learningAlgorithm) {
        this.learningAlgorithm = learningAlgorithm;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    public void setStopAtCondition(Predicate<FeedForwardNetwork> stopAtCondition) {
        this.stopAtCondition = stopAtCondition;
    }

    public void setEnableInfo(boolean enableInfo) {
        this.enableInfo = enableInfo;
    }

    @Override
    public FeedForwardNetwork call() {
        process();
        return this;
    }

    public void trainNetwork() {
        while (!stop) {
            process();
            stop = stopAtCondition.test(this);
        }
    }

    public List<RealVector> testNetwork(Iterable<DataHolder> testData) {
        ArrayList<RealVector> testOutput = new ArrayList<>();
        for (DataHolder testDatum : testData) testOutput.add(feedForward(start, testDatum.getData()));
        return testOutput;
    }

    public RealMatrix[] getWeights() {
        RealMatrix[] layerWeights = new RealMatrix[layerCount];
        int count = 0;
        NetworkLayer layer = start;
        while (null != layer) {
            layerWeights[count] = layer.getWeights();
            count += 1;
            layer = layer.getNextLayer();
        }
        return layerWeights;
    }

    public void setWeights(RealMatrix[] weights) {
        NetworkLayer layer = start;
        int count = 0;
        while (null != layer) {
            layer.setWeights(weights[count]);
            count += 1;
            layer = layer.getNextLayer();
        }
    }

    public RealVector[] getBiases() {
        RealVector[] layerBiases = new RealVector[layerCount];
        int count = 0;
        NetworkLayer layer = start;
        while (null != layer) {
            layerBiases[count] = layer.getBias();
            count += 1;
            layer = layer.getNextLayer();
        }

        return layerBiases;
    }

    public void setBiases(RealVector[] biases) {
        NetworkLayer networkLayer = start;
        int count = 0;
        while (null != networkLayer) {
            networkLayer.setBias(biases[count]);
            count += 1;
            networkLayer = networkLayer.getNextLayer();
        }
    }

    public int getLayerCount() {
        return layerCount;
    }

    public int getEpochCount() {
        return epochCount;
    }

    private void process() {
        int loopCount = (int) Math.ceil((double) recordCount / batchSize);
        double[] error = new double[loopCount];
        int startIndex = 0;
        int endIndex = batchSize;
        for (int i = 0; i < loopCount; i++) {
            dataSet.subList(startIndex, endIndex).forEach(data -> {
                RealVector netOut = feedForward(start, data.getData());
                RealVector errorTerm = calculateError(netOut, data.getPattern());
                learningAlgorithm.trainNetwork(errorTerm, last);
            });
            startIndex = endIndex;
            endIndex = endIndex + batchSize > recordCount ? recordCount - 1 : endIndex + batchSize;
            updateLayerWeights(last);
            updateBias(last);
            error[i] = calculateErrorsPerBatch();
        }
        calculateEpochError(error);
        epochCount += 1;
    }

    private void calculateEpochError(double[] error) {
        double epochErr = Arrays.stream(error).average().getAsDouble();
        if (enableInfo)
            LOGGER.info("{}  -> {} ", errorFunctions.getDesc(), epochErr);
        accumulatedError.add(epochErr);
    }

    private double calculateErrorsPerBatch() {
        double meanError = batchErrors.stream().reduce((x, y) -> x + y).orElse(0.0) / batchErrors.size();
        batchErrors.clear();
        return meanError;
    }

    private void updateBias(NetworkLayer layer) {
        if (null != layer) {
            RealVector summedChange = layer.getDeltaBiasChanges().stream().reduce(RealVector::add).orElseThrow(RuntimeException::new);
            layer.setBias(layer.getBias().subtract(summedChange));
            layer.getDeltaBiasChanges().clear();
            updateBias(layer.getPreviousLayer());
        }
    }

    private void updateLayerWeights(NetworkLayer layer) {
        if (null != layer) {
            RealMatrix summedChange = layer.getDeltaWeightsChange().stream().reduce(RealMatrix::add).orElseThrow(RuntimeException::new);
            layer.setWeights(layer.getWeights().subtract(summedChange));
            layer.getDeltaWeightsChange().clear();
            updateLayerWeights(layer.getPreviousLayer());
        }
    }

    private RealVector calculateError(RealVector layerOut, RealVector pattern) {
        batchErrors.add(errorFunctions.getCostFunction().apply(pattern, layerOut) / layerOut.getDimension());
        return errorFunctions.getCostFunctionDerivative().apply(pattern, layerOut);
    }

    private RealVector feedForward(NetworkLayer layer, RealVector signal) {
        if (null != layer.getNextLayer())
            return feedForward(layer.getNextLayer(), layer.calculateLayerOutput(signal));
        return layer.calculateLayerOutput(signal);
    }

    private void setUpLayers(NetworkLayer layer) {
        if (null != layer) {
            last = layer;
            layerCount += 1;
            setUpLayers(layer.getNextLayer());
        }
    }
}
