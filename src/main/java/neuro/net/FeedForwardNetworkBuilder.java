package neuro.net;

import neuro.data.DataHolder;
import neuro.error.ErrorFunctions;
import neuro.exception.NeuralNetRuntimeException;
import neuro.layer.NetworkLayer;
import neuro.learn.LearningAlgorithm;

import java.util.List;
import java.util.function.Predicate;

public class FeedForwardNetworkBuilder {
    private ErrorFunctions errorFunctions;
    private int batchSize = 1;
    private List<DataHolder> dataSet;
    private NetworkLayer networkLayer;
    private LearningAlgorithm learningAlgorithm;
    private int recordCount = 0;
    private Predicate<FeedForwardNetwork> stopAtCondition;
    private boolean enableInfo = true;

    public FeedForwardNetworkBuilder setErrorFunctions(ErrorFunctions errorFunctions) {
        this.errorFunctions = errorFunctions;
        return this;
    }

    public FeedForwardNetworkBuilder setBatchSize(int batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public FeedForwardNetworkBuilder setDataSet(List<DataHolder> dataSet) {
        this.dataSet = dataSet;
        return this;
    }

    public FeedForwardNetworkBuilder setNetworkLayer(NetworkLayer networkLayer) {
        this.networkLayer = networkLayer;
        return this;
    }

    public FeedForwardNetworkBuilder setLearningAlgorithm(LearningAlgorithm learningAlgorithm) {
        this.learningAlgorithm = learningAlgorithm;
        return this;
    }

    public FeedForwardNetworkBuilder setRecordCount(int recordCount) {
        this.recordCount = recordCount;
        return this;
    }

    public FeedForwardNetworkBuilder setStopAtCondition(Predicate<FeedForwardNetwork> stopAtCondition) {
        this.stopAtCondition = stopAtCondition;
        return this;
    }

    public FeedForwardNetworkBuilder setEnableInfo(boolean enableInfo) {
        this.enableInfo = enableInfo;
        return this;
    }

    public FeedForwardNetwork createFeedForwardNetwork() {
        handleNulls();
        FeedForwardNetwork feedForwardNetwork = new FeedForwardNetwork(networkLayer);
        feedForwardNetwork.setErrorFunctions(errorFunctions);
        feedForwardNetwork.setBatchSize(batchSize);
        feedForwardNetwork.setDataSet(dataSet);
        feedForwardNetwork.setLearningAlgorithm(learningAlgorithm);
        feedForwardNetwork.setRecordCount(recordCount);
        feedForwardNetwork.setStopAtCondition(stopAtCondition);
        feedForwardNetwork.setEnableInfo(enableInfo);
        return feedForwardNetwork;
    }

    private void handleNulls() {
        if (null == networkLayer)
            throw new NeuralNetRuntimeException("Network layers cannot be null");
        if (null == errorFunctions)
            throw new NeuralNetRuntimeException("Error function like MSE or CrossEntropy needs to be selected");
        if (null == dataSet)
            throw new NeuralNetRuntimeException("Iterable data-set cannot be null");
        if (null == learningAlgorithm)
            throw new NeuralNetRuntimeException("Learning algorithm cannot be null");
        if (null == stopAtCondition)
            throw new NeuralNetRuntimeException("Predicate condition to stop training the network must be specified");
    }
}