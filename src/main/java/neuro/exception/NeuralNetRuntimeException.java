package neuro.exception;

public class NeuralNetRuntimeException extends RuntimeException {

    public NeuralNetRuntimeException(String message) {
        super(message);
    }
}
