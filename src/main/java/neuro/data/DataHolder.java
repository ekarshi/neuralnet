package neuro.data;

import org.apache.commons.math3.linear.RealVector;

public class DataHolder {
    private RealVector data;
    private RealVector pattern;

    public DataHolder() {
    }

    public DataHolder(RealVector data, RealVector pattern) {
        this.data = data;
        this.pattern = pattern;
    }

    public RealVector getData() {
        return data;
    }

    public void setData(RealVector data) {
        this.data = data;
    }

    public RealVector getPattern() {
        return pattern;
    }

    public void setPattern(RealVector pattern) {
        this.pattern = pattern;
    }
}
