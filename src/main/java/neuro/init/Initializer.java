package neuro.init;

public interface Initializer {
    double[][] initializeWeights(int inputNeurons, int neurons);

    double[] initializeBias(int neurons);
}
