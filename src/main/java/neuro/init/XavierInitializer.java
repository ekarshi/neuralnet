package neuro.init;


import java.util.Random;

public class XavierInitializer implements Initializer {
    private static final Random RANDOM = new Random();

    public double[][] initializeWeights(int inputNeurons, int neurons) {
        double stdDev = 2.0 / inputNeurons;
        double[][] data = new double[inputNeurons][neurons];
        for (int i = 0; i < inputNeurons; i++)
            for (int j = 0; j < neurons; j++)
                data[i][j] = RANDOM.nextGaussian() * stdDev;
        return data;
    }

    public double[] initializeBias(int neurons) {
        double stdDev = 2.0 / neurons;
        double[] data = new double[neurons];
        for (int i = 0; i < neurons; i++)
            data[i] = RANDOM.nextGaussian() * stdDev;
        return data;
    }
}
