package neuro.init;

import java.util.concurrent.ThreadLocalRandom;

public class RandomInitializer implements Initializer {
    private static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();

    public double[][] initializeWeights(int inputNeurons, int neurons) {
        double[][] weights = new double[inputNeurons][neurons];
        for (int i = 0; i < weights.length; i++) {
            for (int j = 0; j < weights[0].length; j++) {
                weights[i][j] = RANDOM.nextDouble(-1.0, 1.0);
            }
        }
        return weights;
    }

    public double[] initializeBias(int neurons) {
        double[] bias = new double[neurons];
        for (int i = 0; i < bias.length; i++) {
            bias[i] = RANDOM.nextDouble(-1.0, 1.0);
        }
        return bias;
    }
}
