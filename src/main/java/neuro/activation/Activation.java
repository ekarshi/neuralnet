package neuro.activation;

import org.apache.commons.math3.linear.RealVector;

import java.util.function.Function;

public enum Activation {
    RELU(data -> data.map(x -> x < 0 ? 0 : x)
            , data -> data.map(x -> x < 0 ? 0 : 1)),

    PRELU(data -> data.map(x -> x < 0 ? 0.01 * x : x),
            data -> data.map(x -> x < 0 ? 0.01 : 1)),

    SIGMOID(data -> data.map(x -> 1 / (1 + Math.exp(-x))),
            data -> data.map(x -> x * (1 - x))),

    TANH(data -> data.map(Math::tanh), data -> data.map(x -> 1 - Math.pow(x, 2))),

    SOFT_MAX(data -> {
        double max = data.getMaxValue();
        RealVector shifted = data.map(x -> x - max);
        RealVector exp = shifted.map(Math::exp);
        double sum = exp.getL1Norm();
        return exp.map(x -> x / sum);
    }, data -> data.map(x -> x * (1.0 - x))),

    SOFT_PLUS(data -> data.map(x -> Math.log(1 + Math.exp(x))),
            data -> data.map(x -> 1 / (1 + Math.exp(-x))));


    private final Function<RealVector, RealVector> activationFunction;
    private final Function<RealVector, RealVector> activationDerivative;

    Activation(Function<RealVector, RealVector> activationFunction, Function<RealVector, RealVector> activationDerivative) {
        this.activationFunction = activationFunction;
        this.activationDerivative = activationDerivative;
    }

    public Function<RealVector, RealVector> getActivationFunction() {
        return activationFunction;
    }

    public Function<RealVector, RealVector> getDerivative() {
        return activationDerivative;
    }

}
