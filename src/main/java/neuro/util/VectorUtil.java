package neuro.util;

import neuro.exception.NeuralNetRuntimeException;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

public final class VectorUtil {

    private VectorUtil() {
    }

    public static RealVector ebeMultiply(RealVector vectorA, RealVector vectorB) {
        vectorConsistencyCheck(vectorA, vectorB);
        return operation(OPERATION.MUL, vectorA, vectorB);
    }

    public static RealVector ebeDivide(RealVector vectorA, RealVector vectorB) {
        vectorConsistencyCheck(vectorA, vectorB);
        return operation(OPERATION.DIV, vectorA, vectorB);
    }

    private static void vectorConsistencyCheck(RealVector givenA, RealVector givenB) {
        if (null == givenA || null == givenB)
            throw new NeuralNetRuntimeException("Either or both vector cannot be null");
        if (givenA.getDimension() != givenB.getDimension())
            throw new NeuralNetRuntimeException("Vector must match in dimension for arithmetic operation");
    }

    private static RealVector operation(OPERATION operation, RealVector givenA, RealVector givenB) {
        RealVector resultVector = new ArrayRealVector(new double[givenA.getDimension()]);
        for (int i = 0; i < resultVector.getDimension(); i++) {
            if (operation == OPERATION.MUL)
                resultVector.setEntry(i, givenA.getEntry(i) * givenB.getEntry(i));
            if (operation == OPERATION.DIV)
                resultVector.setEntry(i, givenA.getEntry(i) / givenB.getEntry(i));
        }
        return resultVector;
    }

    private enum OPERATION {
        MUL, DIV
    }
}
