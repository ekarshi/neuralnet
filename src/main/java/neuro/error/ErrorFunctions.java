package neuro.error;

import neuro.util.VectorUtil;
import org.apache.commons.math3.linear.RealVector;

import java.util.function.BiFunction;

public enum ErrorFunctions {
    MSE((target, output) -> target.subtract(output).mapToSelf(x -> (x * x) / 2).getL1Norm()
            , (target, output) -> target.subtract(output).mapToSelf(x -> -x), "MSE"),

    CROSS_ENTROPY((target, output) -> VectorUtil.ebeMultiply(output.map(Math::log), target).mapToSelf(x -> -x).getL1Norm(),
            (target, output) -> {
                RealVector out = output.map(x -> x + 1e-8);
                RealVector a = VectorUtil.ebeDivide(target, out).map(x -> -x);
                RealVector b = target.map(x -> 1 - x);
                RealVector c = out.map(x -> 1 - x);
                RealVector bc = VectorUtil.ebeDivide(b, c);
                return a.add(bc);
            }, "CROSS ENTROPY");

    private final BiFunction<RealVector, RealVector, Double> costFunction;
    private final BiFunction<RealVector, RealVector, RealVector> costFunctionDerivative;
    private final String desc;

    ErrorFunctions(BiFunction<RealVector, RealVector, Double> costFunction,
                   BiFunction<RealVector, RealVector, RealVector> costFunctionDerivative,
                   String desc) {
        this.costFunction = costFunction;
        this.costFunctionDerivative = costFunctionDerivative;
        this.desc = desc;
    }

    public BiFunction<RealVector, RealVector, Double> getCostFunction() {
        return costFunction;
    }

    public BiFunction<RealVector, RealVector, RealVector> getCostFunctionDerivative() {
        return costFunctionDerivative;
    }

    public String getDesc() {
        return desc;
    }
}
