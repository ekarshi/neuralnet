package neuro.learn.impl;

import neuro.layer.NetworkLayer;
import neuro.learn.Optimizer;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.HashMap;
import java.util.Map;

public class Adam implements Optimizer {
    private static final double EPSILON = 1e-8;
    private final Map<String, RealMatrix> maGradient;
    private final Map<String, RealMatrix> maGradientSqr;
    private final Map<String, RealVector> maGradientBias;
    private final Map<String, RealVector> maGradientBiasSqr;
    private final double betaMomentum;
    private final double betaRms;
    private final double learningRate;

    public Adam(double betaMomentum, double betaRms, double learningRate) {
        this.betaMomentum = betaMomentum;
        this.betaRms = betaRms;
        this.learningRate = learningRate;
        maGradient = new HashMap<>();
        maGradientSqr = new HashMap<>();
        maGradientBias = new HashMap<>();
        maGradientBiasSqr = new HashMap<>();
    }

    @Override
    public void calculateDeltaWeightChange(NetworkLayer layer) {
        RealMatrix gradient = layer.getPreviousLayerOut().outerProduct(layer.getLayerError());
        RealMatrix emaGradient = ema(gradient, layer, maGradient, betaMomentum);
        RealMatrix emaGradientSqr = ema(squareGradient(gradient.getData()), layer, maGradientSqr, betaRms);
        emaGradient = biasCorrection(emaGradient, betaMomentum, layer.getTimeStep());
        emaGradientSqr = biasCorrection(emaGradientSqr, betaRms, layer.getTimeStep());
        layer.saveDeltaWeightsChange(calculateDeltaChange(emaGradient, emaGradientSqr));
    }

    @Override
    public void calculateDeltaBiasChange(NetworkLayer layer) {
        RealVector gradient = layer.getLayerError();
        RealVector emaGradient = ema(gradient, layer, maGradientBias, betaMomentum);
        RealVector emaGradientSqr = ema(gradient.map(x -> Math.pow(x, 2)), layer, maGradientBiasSqr, betaRms);
        emaGradient = biasCorrection(emaGradient, betaMomentum, layer.getTimeStep());
        emaGradientSqr = biasCorrection(emaGradientSqr, betaRms, layer.getTimeStep());
        layer.saveDeltaBiasChange(calculateDeltaChange(emaGradient, emaGradientSqr));
        layer.setTimeStep(layer.getTimeStep() + 1);
    }

    private RealMatrix calculateDeltaChange(RealMatrix emaData, RealMatrix emaSquaredData) {
        double[][] change = new double[emaData.getRowDimension()][emaData.getColumnDimension()];
        for (int i = 0; i < change.length; i++) {
            for (int j = 0; j < change[0].length; j++)
                change[i][j] = learningRate * (emaData.getEntry(i, j) / Math.sqrt(emaSquaredData.getEntry(i, j) + EPSILON));
        }
        return new Array2DRowRealMatrix(change);
    }

    private RealVector calculateDeltaChange(RealVector emaData, RealVector emaSquaredData) {
        double[] deltaChange = new double[emaData.getDimension()];
        for (int i = 0; i < deltaChange.length; i++)
            deltaChange[i] = learningRate * (emaData.getEntry(i) / Math.sqrt(emaSquaredData.getEntry(i) + EPSILON));
        return new ArrayRealVector(deltaChange);
    }

    private RealMatrix biasCorrection(RealMatrix emaData, double beta, int timeStep) {
        return emaData.scalarMultiply(1.0 / (1.0 - Math.pow(beta, timeStep)));
    }

    private RealVector biasCorrection(RealVector emaData, double beta, int timeStep) {
        return emaData.mapMultiply(1.0 / (1.0 - Math.pow(beta, timeStep)));
    }
}
