package neuro.learn.impl;

import neuro.layer.NetworkLayer;
import neuro.learn.Optimizer;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.HashMap;
import java.util.Map;

public class MomentumOptimizer implements Optimizer {

    private final double momentumFactor;
    private final double learningRate;
    private final Map<String, RealMatrix> pastGradient;
    private final Map<String, RealVector> pastBiasGradient;

    public MomentumOptimizer(double momentumFactor, double learningRate) {
        this.momentumFactor = momentumFactor;
        this.learningRate = learningRate;
        this.pastGradient = new HashMap<>();
        this.pastBiasGradient = new HashMap<>();
    }

    @Override
    public void calculateDeltaWeightChange(NetworkLayer layer) {
        RealMatrix deltaWeightChanges = layer.getPreviousLayerOut().outerProduct(layer.getLayerError()).scalarMultiply(learningRate);
        if (null != pastGradient.get(layer.getLayerId()))
            deltaWeightChanges = pastGradient.get(layer.getLayerId()).scalarMultiply(momentumFactor).add(deltaWeightChanges);
        pastGradient.put(layer.getLayerId(), deltaWeightChanges);
        layer.saveDeltaWeightsChange(deltaWeightChanges);
    }

    @Override
    public void calculateDeltaBiasChange(NetworkLayer layer) {
        RealVector deltaBiasChanges = layer.getLayerError().mapMultiply(learningRate);
        if (null != pastBiasGradient.get(layer.getLayerId()))
            deltaBiasChanges = pastBiasGradient.get(layer.getLayerId()).map(x -> x * momentumFactor).add(deltaBiasChanges);
        pastBiasGradient.put(layer.getLayerId(), deltaBiasChanges);
        layer.saveDeltaBiasChange(deltaBiasChanges);
    }
}
