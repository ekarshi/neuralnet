package neuro.learn.impl;

import neuro.layer.NetworkLayer;
import neuro.learn.Optimizer;
import neuro.util.VectorUtil;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.HashMap;
import java.util.Map;

public class RMSPropOptimizer implements Optimizer {

    private static final double EPSILON = 1e-8;
    private final Map<String, RealMatrix> squaredGradients;
    private final Map<String, RealVector> squaredBias;
    private final double learningRate;
    private final double mu;

    public RMSPropOptimizer(double learningRate, double smoothingParam) {
        this.learningRate = learningRate;
        this.mu = smoothingParam;
        this.squaredGradients = new HashMap<>();
        this.squaredBias = new HashMap<>();
    }

    @Override
    public void calculateDeltaWeightChange(NetworkLayer layer) {
        RealMatrix gradient = layer.getPreviousLayerOut().outerProduct(layer.getLayerError());
        RealMatrix squaredGradient = squareGradient(gradient.getData());
        RealMatrix emaGradientWeight = ema(squaredGradient, layer, squaredGradients, mu);
        layer.saveDeltaWeightsChange(calculateChanges(emaGradientWeight.getData(), gradient.getData()));
    }

    @Override
    public void calculateDeltaBiasChange(NetworkLayer layer) {
        RealVector gradient = layer.getLayerError();
        RealVector squaredGradient = gradient.map(x -> Math.pow(x, 2));
        RealVector emaGradientBias = ema(squaredGradient, layer, squaredBias, mu);
        layer.saveDeltaBiasChange(calculateChanges(emaGradientBias, gradient));
    }

    private RealVector calculateChanges(RealVector emaGradientBias, RealVector gradientBias) {
        return VectorUtil.ebeMultiply(emaGradientBias.map(x -> learningRate / Math.sqrt(x + EPSILON)), gradientBias);
    }

    private RealMatrix calculateChanges(double[][] emaGradient, double[][] currentGradient) {
        for (int i = 0; i < emaGradient.length; i++) {
            for (int j = 0; j < emaGradient[0].length; j++) {
                currentGradient[i][j] = currentGradient[i][j] * learningRate / Math.sqrt(emaGradient[i][j] + EPSILON);
            }
        }
        return new Array2DRowRealMatrix(currentGradient);
    }
}
