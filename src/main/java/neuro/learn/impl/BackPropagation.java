package neuro.learn.impl;

import neuro.layer.LayerType;
import neuro.layer.NetworkLayer;
import neuro.learn.LearningAlgorithm;
import neuro.learn.Optimizer;
import neuro.util.VectorUtil;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public final class BackPropagation implements LearningAlgorithm {

    private double learningRate;
    private Optimizer optimizer;

    public BackPropagation(double learningRate) {
        this.learningRate = learningRate;
    }

    public BackPropagation(Optimizer optimizer) {
        this.optimizer = optimizer;
    }

    @Override
    public void trainNetwork(RealVector error, NetworkLayer layer) {
        backPropagateError(error, layer);
    }

    @Override
    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    private void backPropagateError(RealVector error, NetworkLayer layer) {
        if (null != layer) {
            if (layer.getLayerType() == LayerType.OUT_LAYER)
                layer.setLayerError(VectorUtil.ebeMultiply(error, layer.activationDerivative()));
            else {
                RealMatrix weights = layer.getNextLayer().getWeights().transpose();
                layer.setLayerError(VectorUtil.ebeMultiply(layer.activationDerivative(), weights.preMultiply(error)));
            }
            if (null == optimizer) {
                deltaWeightChanges(layer);
                deltaBiasChange(layer);
            } else {
                optimizer.calculateDeltaWeightChange(layer);
                optimizer.calculateDeltaBiasChange(layer);
            }
            backPropagateError(layer.getLayerError(), layer.getPreviousLayer());
        }
    }

    private void deltaWeightChanges(NetworkLayer layer) {
        RealMatrix deltaWeightChanges = layer.getPreviousLayerOut().outerProduct(layer.getLayerError()).scalarMultiply(learningRate);
        layer.saveDeltaWeightsChange(deltaWeightChanges);
    }

    private void deltaBiasChange(NetworkLayer layer) {
        RealVector deltaBiasChanges = layer.getLayerError().mapMultiply(learningRate);
        layer.saveDeltaBiasChange(deltaBiasChanges);
    }
}
