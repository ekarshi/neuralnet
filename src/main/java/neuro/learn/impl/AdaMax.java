package neuro.learn.impl;

import neuro.layer.NetworkLayer;
import neuro.learn.Optimizer;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.HashMap;
import java.util.Map;

public class AdaMax implements Optimizer {
    private final Map<String, RealMatrix> emaGradientWeight;
    private final Map<String, RealMatrix> emaGradientWeightNorm;
    private final Map<String, RealVector> emaGradientBias;
    private final Map<String, RealVector> getEmaGradientBiasNorm;
    private final double learningRate;
    private final double betaMomentum;
    private final double betaInfNorm;

    public AdaMax(double learningRate, double betaMomentum, double betaInfNorm) {
        this.learningRate = learningRate;
        this.betaMomentum = betaMomentum;
        this.betaInfNorm = betaInfNorm;
        emaGradientWeight = new HashMap<>();
        emaGradientWeightNorm = new HashMap<>();
        emaGradientBias = new HashMap<>();
        getEmaGradientBiasNorm = new HashMap<>();
    }

    @Override
    public void calculateDeltaWeightChange(NetworkLayer layer) {
        RealMatrix gradient = layer.getPreviousLayerOut().outerProduct(layer.getLayerError());
        RealMatrix emaGradient = ema(gradient, layer, emaGradientWeight, betaMomentum);
        RealMatrix infNorm = abs(gradient);
        if (emaGradientWeightNorm.containsKey(layer.getLayerId()))
            infNorm = max(infNorm, emaGradientWeightNorm.get(layer.getLayerId()));
        emaGradientWeightNorm.put(layer.getLayerId(), infNorm);
        emaGradient = biasCorrection(emaGradient, betaMomentum, layer.getTimeStep());
        layer.saveDeltaWeightsChange(calculateDeltaChange(emaGradient, infNorm));
    }

    @Override
    public void calculateDeltaBiasChange(NetworkLayer layer) {
        RealVector gradient = layer.getLayerError();
        RealVector emaGradient = ema(gradient, layer, emaGradientBias, betaMomentum);
        RealVector infNorm = gradient.map(Math::abs);
        if (getEmaGradientBiasNorm.containsKey(layer.getLayerId()))
            infNorm = max(infNorm, getEmaGradientBiasNorm.get(layer.getLayerId()));
        getEmaGradientBiasNorm.put(layer.getLayerId(), infNorm);
        emaGradient = biasCorrection(emaGradient, betaMomentum, layer.getTimeStep());
        layer.saveDeltaBiasChange(calculateDeltaChange(emaGradient, infNorm));
        layer.setTimeStep(layer.getTimeStep() + 1);
    }

    private RealVector calculateDeltaChange(RealVector biasCorrectedTerm, RealVector infNorm) {
        double[] deltaChange = new double[biasCorrectedTerm.getDimension()];
        for (int i = 0; i < deltaChange.length; i++)
            deltaChange[i] = learningRate * biasCorrectedTerm.getEntry(i) / infNorm.getEntry(i);

        return new ArrayRealVector(deltaChange);
    }

    private RealMatrix calculateDeltaChange(RealMatrix biasCorrectedTerm, RealMatrix infNorm) {
        double[][] deltaChange = new double[biasCorrectedTerm.getRowDimension()][biasCorrectedTerm.getColumnDimension()];
        for (int i = 0; i < deltaChange.length; i++) {
            for (int j = 0; j < deltaChange[0].length; j++) {
                deltaChange[i][j] = learningRate * biasCorrectedTerm.getEntry(i, j) / infNorm.getEntry(i, j);
            }
        }
        return new Array2DRowRealMatrix(deltaChange);
    }

    private RealMatrix max(RealMatrix absGradient, RealMatrix prevGradient) {
        double[][] val = new double[absGradient.getRowDimension()][absGradient.getColumnDimension()];
        for (int i = 0; i < val.length; i++)
            for (int j = 0; j < val[0].length; j++)
                val[i][j] = absGradient.getEntry(i, j) > betaInfNorm * prevGradient.getEntry(i, j) ?
                        absGradient.getEntry(i, j) : betaInfNorm * prevGradient.getEntry(i, j);

        return new Array2DRowRealMatrix(val);
    }

    private RealVector max(RealVector absGradient, RealVector prevGradient) {
        double[] val = new double[absGradient.getDimension()];
        for (int i = 0; i < val.length; i++) {
            val[i] = absGradient.getEntry(i) > betaInfNorm * prevGradient.getEntry(i) ?
                    absGradient.getEntry(i) : betaInfNorm * prevGradient.getEntry(i);
        }
        return new ArrayRealVector(val);
    }

    private RealMatrix abs(RealMatrix data) {
        double[][] absData = new double[data.getRowDimension()][data.getColumnDimension()];
        for (int i = 0; i < absData.length; i++) {
            for (int j = 0; j < absData[0].length; j++)
                absData[i][j] = Math.abs(data.getEntry(i, j));
        }
        return new Array2DRowRealMatrix(absData);
    }

    private RealMatrix biasCorrection(RealMatrix gradient, double beta, double timeStep) {
        return gradient.scalarMultiply(1.0 / (1.0 - Math.pow(beta, timeStep)));
    }

    private RealVector biasCorrection(RealVector emaData, double beta, int timeStep) {
        return emaData.mapMultiply(1.0 / (1.0 - Math.pow(beta, timeStep)));
    }
}
