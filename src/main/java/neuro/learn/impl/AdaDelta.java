package neuro.learn.impl;

import neuro.layer.NetworkLayer;
import neuro.learn.Optimizer;
import neuro.util.VectorUtil;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.HashMap;
import java.util.Map;

public class AdaDelta implements Optimizer {
    private static final double EPSILON = 1e-8;
    private final Map<String, RealMatrix> emaGradientWeight;
    private final Map<String, RealMatrix> emaGradientWeightUpdate;
    private final Map<String, RealVector> emaGradientBias;
    private final Map<String, RealVector> emaGradientBiasUpdate;
    private final double smoothingParam;

    public AdaDelta(double smoothingParam) {
        this.smoothingParam = smoothingParam;
        emaGradientWeight = new HashMap<>();
        emaGradientWeightUpdate = new HashMap<>();
        emaGradientBias = new HashMap<>();
        emaGradientBiasUpdate = new HashMap<>();
    }

    @Override
    public void calculateDeltaWeightChange(NetworkLayer layer) {
        RealMatrix gradient = layer.getPreviousLayerOut().outerProduct(layer.getLayerError());
        RealMatrix squaredGradient = squareGradient(gradient.getData());
        RealMatrix emaGradient = ema(squaredGradient, layer, emaGradientWeight, smoothingParam);
        RealMatrix deltaUpdate;
        if (null != emaGradientWeightUpdate.get(layer.getLayerId()))
            deltaUpdate = calculateDeltaChange(emaGradientWeightUpdate.get(layer.getLayerId()), emaGradient, gradient);
        else
            deltaUpdate = calculateDeltaChange(new Array2DRowRealMatrix(new double[emaGradient.getRowDimension()][emaGradient.getColumnDimension()]), emaGradient, gradient);
        ema(squareGradient(deltaUpdate.getData()), layer, emaGradientWeightUpdate, smoothingParam);
        layer.saveDeltaWeightsChange(deltaUpdate);
    }

    @Override
    public void calculateDeltaBiasChange(NetworkLayer layer) {
        RealVector gradient = layer.getLayerError();
        RealVector squaredGradient = gradient.map(x -> Math.pow(x, 2));
        RealVector emaGradient = ema(squaredGradient, layer, emaGradientBias, smoothingParam);
        RealVector deltaUpdate;
        if (null != emaGradientBiasUpdate.get(layer.getLayerId()))
            deltaUpdate = calculateDeltaChange(emaGradientBiasUpdate.get(layer.getLayerId()), emaGradient, gradient);
        else
            deltaUpdate = calculateDeltaChange(new ArrayRealVector(new double[emaGradient.getDimension()]), emaGradient, gradient);
        ema(deltaUpdate.map(x -> Math.pow(x, 2)), layer, emaGradientBiasUpdate, smoothingParam);
        layer.saveDeltaBiasChange(deltaUpdate);
    }

    private RealVector calculateDeltaChange(RealVector updateTerm, RealVector emaGradient, RealVector gradient) {
        RealVector sqrtUpdate = updateTerm.map(x -> Math.sqrt(x + EPSILON));
        RealVector sqrtEma = emaGradient.map(x -> Math.sqrt(x + EPSILON));
        return VectorUtil.ebeMultiply(VectorUtil.ebeDivide(sqrtUpdate, sqrtEma), gradient);
    }

    private RealMatrix calculateDeltaChange(RealMatrix updateTerm, RealMatrix emaGradient, RealMatrix gradient) {
        double[][] data = new double[updateTerm.getRowDimension()][updateTerm.getColumnDimension()];
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[0].length; j++) {
                data[i][j] = Math.sqrt(updateTerm.getEntry(i, j) + EPSILON) /
                        Math.sqrt(emaGradient.getEntry(i, j) + EPSILON) * gradient.getEntry(i, j);
            }
        }
        return new Array2DRowRealMatrix(data);
    }
}
