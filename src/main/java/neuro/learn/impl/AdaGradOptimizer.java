package neuro.learn.impl;

import neuro.layer.NetworkLayer;
import neuro.learn.Optimizer;
import neuro.util.VectorUtil;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.HashMap;
import java.util.Map;

public class AdaGradOptimizer implements Optimizer {

    private static final double EPSILON = 1e-8;
    private final Map<String, RealMatrix> squaredGradientsWeights;
    private final Map<String, RealVector> squaredGradientsBias;
    private final double learningRate;

    public AdaGradOptimizer(double learningRate) {
        this.learningRate = learningRate;
        this.squaredGradientsWeights = new HashMap<>();
        this.squaredGradientsBias = new HashMap<>();
    }

    @Override
    public void calculateDeltaWeightChange(NetworkLayer layer) {
        RealMatrix gradient = layer.getPreviousLayerOut().outerProduct(layer.getLayerError());
        RealMatrix squaredGradient = squareGradient(gradient.getData());
        if (squaredGradientsWeights.containsKey(layer.getLayerId()))
            squaredGradient = squaredGradientsWeights.get(layer.getLayerId()).add(squaredGradient);
        squaredGradientsWeights.put(layer.getLayerId(), squaredGradient);
        layer.saveDeltaWeightsChange(calculate(squaredGradient.getData(), gradient.getData()));
    }

    @Override
    public void calculateDeltaBiasChange(NetworkLayer layer) {
        RealVector gradient = layer.getLayerError();
        RealVector squaredGradient = gradient.map(x -> Math.pow(x, 2));
        if (squaredGradientsBias.containsKey(layer.getLayerId()))
            squaredGradient = squaredGradient.add(squaredGradientsBias.get(layer.getLayerId()));
        squaredGradientsBias.put(layer.getLayerId(), squaredGradient);
        RealVector deltaBias = VectorUtil.ebeMultiply(squaredGradient.map(x -> x + EPSILON).map(x -> learningRate / Math.sqrt(x)), gradient);
        layer.saveDeltaBiasChange(deltaBias);
    }

    private RealMatrix calculate(double[][] data, double[][] gradient) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[0].length; j++)
                gradient[i][j] = gradient[i][j] * (learningRate / Math.sqrt(data[i][j] + EPSILON));
        }
        return new Array2DRowRealMatrix(gradient);
    }
}
