package neuro.learn;

import neuro.layer.NetworkLayer;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.Map;

public interface Optimizer {

    void calculateDeltaWeightChange(NetworkLayer layer);

    void calculateDeltaBiasChange(NetworkLayer layer);

    default RealMatrix squareGradient(double[][] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[0].length; j++) {
                data[i][j] = Math.pow(data[i][j], 2);
            }
        }
        return new Array2DRowRealMatrix(data);
    }

    default RealMatrix ema(RealMatrix data, NetworkLayer layer, Map<String, RealMatrix> ref, double beta) {
        RealMatrix ema = data.scalarMultiply(1.0 - beta);
        if (null != ref.get(layer.getLayerId()))
            ema = ref.get(layer.getLayerId()).scalarMultiply(beta).add(ema);
        ref.put(layer.getLayerId(), ema);
        return ema;
    }

    default RealVector ema(RealVector data, NetworkLayer layer, Map<String, RealVector> ref, double beta) {
        RealVector ema = data.mapMultiply(1.0 - beta);
        if (null != ref.get(layer.getLayerId()))
            ema = ref.get(layer.getLayerId()).mapMultiply(beta).add(ema);
        ref.put(layer.getLayerId(), ema);
        return ema;
    }
}
