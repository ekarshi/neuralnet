package neuro.learn;

import neuro.layer.NetworkLayer;
import org.apache.commons.math3.linear.RealVector;

public interface LearningAlgorithm {

    void trainNetwork(RealVector error, NetworkLayer layer);

    void setLearningRate(double learningRate);
}
