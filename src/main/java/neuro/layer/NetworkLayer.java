package neuro.layer;

import neuro.activation.Activation;
import neuro.init.Initializer;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class NetworkLayer {
    private final List<RealMatrix> deltaWeights;
    private final List<RealVector> deltaBias;
    private final LayerType layerType;
    private final Activation activation;
    private final String layerId = UUID.randomUUID().toString();
    private RealMatrix weights;
    private RealVector bias;
    private RealVector output;
    private NetworkLayer nextLayer;
    private NetworkLayer previousLayer;
    private RealVector layerError;
    private RealVector prevLayerOut;
    private int timeStep = 1;

    public NetworkLayer(int neurons, int connections, Activation activation, LayerType layerType, Initializer initializer) {
        this.weights = MatrixUtils.createRealMatrix(initializer.initializeWeights(connections, neurons));
        this.bias = new ArrayRealVector(initializer.initializeBias(neurons));
        this.activation = activation;
        this.layerType = layerType;
        this.deltaWeights = new ArrayList<>();
        this.deltaBias = new ArrayList<>();
    }

    public RealVector calculateLayerOutput(RealVector inputSignal) {
        prevLayerOut = inputSignal;
        RealVector input = bias.add(weights.preMultiply(inputSignal));
        output = activation.getActivationFunction().apply(input);
        return output;
    }

    public LayerType getLayerType() {
        return this.layerType;
    }

    public RealMatrix getWeights() {
        return this.weights;
    }

    public void setWeights(RealMatrix weights) {
        this.weights = weights;
    }

    public RealVector getBias() {
        return this.bias;
    }

    public void setBias(RealVector bias) {
        this.bias = bias;
    }

    public NetworkLayer getNextLayer() {
        return nextLayer;
    }

    public void setNextLayer(NetworkLayer layer) {
        this.nextLayer = layer;
    }

    public NetworkLayer getPreviousLayer() {
        return previousLayer;
    }

    public void setPreviousLayer(NetworkLayer layer) {
        this.previousLayer = layer;
    }

    public RealVector activationDerivative() {
        return activation.getDerivative().apply(output);
    }

    public RealVector getLayerError() {
        return layerError;
    }

    public void setLayerError(RealVector error) {
        this.layerError = error;
    }

    public void saveDeltaWeightsChange(RealMatrix weights) {
        this.deltaWeights.add(weights);
    }

    public RealVector getPreviousLayerOut() {
        return prevLayerOut;
    }

    public void saveDeltaBiasChange(RealVector biasChange) {
        this.deltaBias.add(biasChange);
    }

    public List<RealVector> getDeltaBiasChanges() {
        return this.deltaBias;
    }

    public List<RealMatrix> getDeltaWeightsChange() {
        return this.deltaWeights;
    }

    public String getLayerId() {
        return layerId;
    }

    public int getTimeStep() {
        return timeStep;
    }

    public void setTimeStep(int timeStep) {
        this.timeStep = timeStep;
    }
}
