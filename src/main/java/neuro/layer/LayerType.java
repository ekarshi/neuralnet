package neuro.layer;

public enum LayerType {
    HIDDEN_LAYER,
    OUT_LAYER
}
