package example;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String trainingPath = "train_scaled.data";
        String testPath = "test_scaled.data";
        SerialBackPropagation serialBackPropagation = new SerialBackPropagation(trainingPath, testPath);
        serialBackPropagation.train();
    }
}
