package example;

import com.sun.org.apache.bcel.internal.util.ClassLoader;
import neuro.data.DataHolder;
import org.apache.commons.io.IOUtils;
import org.apache.commons.math3.linear.ArrayRealVector;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class DataLoader {
    private final List<DataHolder> data;
    private List<String> lines;

    public DataLoader(String fileName, int patternCount, String regexp) throws IOException {
        lines = IOUtils.readLines(new InputStreamReader(ClassLoader.getSystemResourceAsStream(fileName), StandardCharsets.UTF_8));
        Collections.shuffle(lines);
        data = new ArrayList<>();
        parseToDataHolder(patternCount, regexp);
    }

    public List<DataHolder> getData() {
        return data;
    }

    private void parseToDataHolder(int patternCount, String regex) {
        for (String lines : lines) {
            String[] cols = lines.split(regex);
            createDataHolder(cols, patternCount);
        }
    }

    private void createDataHolder(String[] cols, int patternCount) {
        double[] dataD = new double[cols.length - patternCount];
        double[] pattern = new double[patternCount];
        int innerCount = 0;
        for (int i = 0; i < cols.length; i++) {
            if (i >= cols.length - patternCount) {
                pattern[innerCount] = Double.parseDouble(cols[i]);
                innerCount += 1;
            } else
                dataD[i] = Double.parseDouble(cols[i]);
        }
        DataHolder holder = new DataHolder(new ArrayRealVector(dataD), new ArrayRealVector(pattern));
        this.data.add(holder);
    }
}
