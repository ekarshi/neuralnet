package example;

import neuro.activation.Activation;
import neuro.data.DataHolder;
import neuro.error.ErrorFunctions;
import neuro.init.XavierInitializer;
import neuro.layer.LayerType;
import neuro.layer.NetworkLayer;
import neuro.learn.impl.BackPropagation;
import neuro.learn.impl.MomentumOptimizer;
import neuro.net.FeedForwardNetwork;
import neuro.net.FeedForwardNetworkBuilder;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.IntStream;

class SerialBackPropagation {
    private static final Logger LOGGER = LogManager.getFormatterLogger(SerialBackPropagation.class);
    private List<DataHolder> testData;
    private final Predicate<FeedForwardNetwork> stoppingCondition = network -> {
        List<RealVector> testResult = network.testNetwork(testData);
        return compareResults(testResult, testData) >= 0.9;
    };
    private List<DataHolder> trainData;
    private FeedForwardNetwork network;

    public SerialBackPropagation(String trainingDataPath, String testDataPath) throws IOException {
        setUpData(trainingDataPath, testDataPath);
        network = createNetwork();
    }

    private static NetworkLayer setUpLayers() {
        NetworkLayer hiddenLayer = new NetworkLayer(50, 36, Activation.PRELU, LayerType.HIDDEN_LAYER, new XavierInitializer());
        NetworkLayer outputLayer = new NetworkLayer(6, 50, Activation.SOFT_MAX, LayerType.OUT_LAYER, new XavierInitializer());
        hiddenLayer.setPreviousLayer(null);
        hiddenLayer.setNextLayer(outputLayer);
        outputLayer.setPreviousLayer(hiddenLayer);
        outputLayer.setNextLayer(null);
        return hiddenLayer;
    }

    private static double compareResults(List<RealVector> prediction, List<DataHolder> data) {
        long hitCount = IntStream.range(0, prediction.size()).
                filter(index -> prediction.get(index).getMaxIndex() == data.get(index).getPattern().getMaxIndex()).count();
        LOGGER.printf(Level.INFO, "Accuracy => %.2f %%", hitCount / (double) prediction.size() * 100);
        return hitCount / (double) prediction.size();
    }

    public void train() {
        network.trainNetwork();
    }

    private void setUpData(String trainDataSetPath, String testDataSetPath) throws IOException {
        DataLoader trainDataLoader = new DataLoader(trainDataSetPath, 6, ",");
        trainData = trainDataLoader.getData();
        Collections.shuffle(trainData);
        DataLoader testDataLoader = new DataLoader(testDataSetPath, 6, ",");
        testData = testDataLoader.getData();
    }

    private FeedForwardNetwork createNetwork() {
        MomentumOptimizer momentumOptimizer = new MomentumOptimizer(0.9, 0.001);
        BackPropagation backPropagation = new BackPropagation(momentumOptimizer);
        return new FeedForwardNetworkBuilder().
                setErrorFunctions(ErrorFunctions.CROSS_ENTROPY).
                setBatchSize(10).
                setDataSet(trainData).
                setNetworkLayer(setUpLayers()).
                setLearningAlgorithm(backPropagation).
                setRecordCount(trainData.size()).
                setStopAtCondition(stoppingCondition).
                setEnableInfo(true).createFeedForwardNetwork();
    }
}
